package enve_test

import (
	"fmt"
	"log"
	"net"
	"os"

	"gitlab.com/efronlicht/enve"
)

// Use the Or family of functions for optional environment variables.
func Example_or() {
	port := fmt.Sprintf(":%d", enve.IntOr("PORT", 8080)) // default to 8080 if not set
	fmt.Printf("server listening at %s\n", port)
	// Output:
	// server listening at :8080
}

func Example_unit() {
	os.Setenv("MAX_CACHE_SIZE", "32 GiB")
	fmt.Println(enve.MustIntUnit("MAX_CACHE_SIZE"))
	// Output: 34359738368
}

/*
func Example_slog() {
	os.Setenv("ENVE_LOG_SLOG", "1")
	defer replaceStderr()()
	slog.SetDefault(slog.New(slog.NewJSONHandler(os.Stdout, &slog.HandlerOptions{
		ReplaceAttr: func(groups []string, a slog.Attr) slog.Attr {
			if a.Key == "time" {
				return slog.Attr{}
			}
			return a
		},
	})))

	_ = enve.IntOr("PORT", 8080) // default to 8080 if not set
	// Output:
	// {"level":"INFO","msg":"enve: missing optional envvar: falling back to default","enve":{"key":"PORT","caller":"enve/example_test.go:33 enve_test.Example_slog","default":8080,"parser":"enve/internal/parse/parse.go:8 enve/internal/parse.Int"}}
}*/

// Use the Must family of functions for required environment variables.
func Example_must() {
	os.Setenv("THE_ANSWER", "42")
	fmt.Println(enve.MustInt("THE_ANSWER"))
	// Output: 42
}

func Example_mustPanicOnMissing() {
	defer replaceStderr()()
	defer func() { recover() }()

	fmt.Println(enve.MustString("THE_QUESTION"))
	// enve: enve/example_test.go:48 enve_test.Example_mustPanicOnMissing FATAL ERR: missing required envvar THE_QUESTION
}

func replaceStderr() func() {
	log.SetFlags(0)

	oldstderr := *os.Stderr
	*os.Stderr = *os.Stdout // redirect stderr to stdout for testing purposes to show enve's diagnostic messages
	return func() { *os.Stderr = oldstderr }
}

// You can use your own parser functions with Or or Must for custom types.
func Example_orCustom() {
	parseIP := func(s string) (net.IP, error) {
		if ip := net.ParseIP(s); ip != nil {
			return ip, nil
		}
		return nil, fmt.Errorf("invalid IP address %q", s)
	}

	os.Setenv("DNS", "8.8.8.8")                                // google's public DNS
	fmt.Println(enve.Or(parseIP, "DNS", net.IPv4(1, 1, 1, 1))) // default to Cloudflare's public DNS if not set
	// Output: 8.8.8.8
}
