# enve v1.2.2

[![GoDoc](./goreferencebanner.svg)](https://pkg.go.dev/gitlab.com/efronlicht/enve)


Parsing and configuration via environment variables.

- Handy logs & error messages so you can easily discover your configuration by just running the program.
- Lets you know by default what knobs are required or missing... at most once per environment variable.
- As simple an API as I could make.

## the big idea

### Mandatory Environment Variables

If you have a **required** configuration value, use the **Must** family of functions:
- [`MustString`](https://pkg.go.dev/gitlab.com/efronlicht/enve#MustString)
- [`MustInt`](https://pkg.go.dev/gitlab.com/efronlicht/enve#MustInt)
- [`MustBool`](https://pkg.go.dev/gitlab.com/efronlicht/enve#MustBool)
- ...

Or specify _custom_ parsing functions with [`Must`](https://pkg.go.dev/gitlab.com/efronlicht/enve#Must) itself.

### Optional Environment Variables

If you have an **optional** configuration value, use the **Or** family of functions:
- [`StringOr`](https://pkg.go.dev/gitlab.com/efronlicht/enve#StringOr)
- [`IntOr`](https://pkg.go.dev/gitlab.com/efronlicht/enve#IntOr)
- [`BoolOr`](https://pkg.go.dev/gitlab.com/efronlicht/enve#BoolOr)
- ...

Or specify _custom_ parsing functions with [`Or`](https://pkg.go.dev/gitlab.com/efronlicht/enve#Or) itself.



## Customizing Enve's Logging Output
Enve outputs logs using the `log` package by default, using it's default (global) logger. You can customize this behavior using environment variables:
- Use `ENVE_LOG_SLOG=1` to use the `log/slog` package instead: it will use the default (global) logger at INFO level.
Enve tries to provide the 'right amount' of logging, but you can tweak it a bit using the [envvars described in the consts section of the documentation](https://pkg.go.dev/gitlab.com/efronlicht/enve#pkg-constants).




See more [examples](http://localhost:6060/pkg/gitlab.com/efronlicht/enve/#example_Must) in the [![GoDoc](./goreferencebanner.svg)](https://pkg.go.dev/gitlab.com/efronlicht/enve).
